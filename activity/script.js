// Activity:

// 3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
const getCube = 2 ** 3;

// 4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
console.log(`The cube of 2 is ${getCube}`);

// 5. Create a variable address with a value of an array containing details of an address.
const address = ["258 Washington Ave NW", " California 90011" ]
// 6. Destructure the array and print out a message with the full address using Template Literals.
console.log(`I live at ${address}`);

// 7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
const animanlDetails = {
	animal1: 'Lolong',
	animal2: 'crocodile',
	kgs: 1075,
	measure: 20,
	ft: 3
}

// 8. Destructure the object and print out a message with the details of the animal using Template Literals.
const { animal1, animal2, kgs, measure, ft} = animanlDetails
console.log(`${animal1} was a saltwater ${animal2}. He weighed at ${kgs} kgs with a measurement of ${measure} ft ${ft} in.`)

// 9. Create an array of numbers.
const arrayNum = [1, 2, 3, 4, 5];

// 10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
arrayNum.forEach(function(index){
console.log(index)});

// 11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
const sum = (arrayNum) => arrayNum
let newReduce = sum(1+2+3+4+5)
console.log(newReduce)

// /*12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
class dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

dog.name = 'Coco';
dog.age = 7;
dog.breed = 'Labrador Retriever';

// 13. Create/instantiate a new object from the class Dog and console log the object.
const myDog = new dog('Coco', '7', 'Labrador Retriever')
console.log(myDog)
